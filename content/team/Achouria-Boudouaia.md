---
title: "Achouria BOUDOUAIA"
date: 2018-09-29T14:42:07+06:00
description : "achouria boubouaia transloma transitaire à marseille"
designation: Présidente
image : images/team/team-page-6.jpg
bgImage: images/background/page-title.jpg
facebookURL : '#'
twitterURL : '#'
linkedinURL : '#'
googlePlusURL : '#'
projectDone : 32
successRate : 100%
experienceOf : 33 ans
from : Marseille, FRANCE
cvURL : '#'
mobile : +33 6 61 49 12 92
email : achouria.transloma@sfr.fr
location : 24 RUE CHARLES TELLIER,13014 MARSEILLE.
description : TEL :04.65.85.07.64 Siret :842 572 232 00010
branding : 85%
consulting : 90%
business : 75%
type : team
---

#### Expérience

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis.

#### Profile Analytics

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.