---
title: "Lieux de départ"
date: 2018-09-29T11:51:17+06:00
description : "services  aeroports ports régions désservies par transloma "
image: images/service/maritime-3.png
bgImage: images/background/page-title.jpg
icon: ti-anchor
brochureURL: '#'
regularDay: Lun-Ven
regularTime: 08:00 - 18.00
halfDay: Samedi
halfTime: 09:00 - 12:00
offDay: Dimanche
type : service
---

### Les services et ports de départ

Pour votre import ou export de marchandises.


>En fonction du type de vos marchandises et de vos impératifs, TRANSLOMA étudiera et vous proposera les meilleures solutions d’acheminement.

1. Marseille : Région Provence Alpes Cote d’Azur, Sud Ouest de la France, Région Lyonnaise, Suisse, Italie, Nord de l’Espagne, Afrique du Nord.
2. Anvers/Rotterdam : Paris et Nord de Paris, Nord et Est de la France, Belgique, Pays Bas, Allemagne, Luxembourg, Suisse.
3. Alger : Pour les expéditions directes.
4. Le Havre: Région Parisienne, Nord Ouest de la France, Région Lyonnaise.
5. Depuis toutes provenances ou vers toutes destinations, vers le Maghreb et spécialement l’Algérie



