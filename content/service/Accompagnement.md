---
title: "Accompagnement"
date: 2018-09-29T11:54:02+06:00
description : "ENTREPOSAGE  MANUTENTION EMPOTAGE  DEPOTAGE • GROUPAGE  DEGROUPAGE DOUANE"
image: images/service/maritime-2.png
bgImage: images/background/page-title.jpg
icon: ti-package
brochureURL: '#'
regularDay: Lun-Ven
regularTime: 08:00 - 18.00
halfDay: Samedi
halfTime: 09:00 - 12:00
offDay: Dimanche
type : service
---


### L’expertise de notre personnel
 spécialisée dans le transport maritime de marchandises mondiales



>La gestion/réalisation de projets industriels expédiés par voie maritime,
Nous vous proposons des services complets,les expéditions de nos services fret mer international comprennent le chargement et l’emballage des marchandises, la consolidation des commandes et la gestion de projets pour les engins de levage lourds. Ajoutez à cela notre capacité à répondre aux exigences internationales en matière d’expéditions maritimes du pays d’origine au pays de destination.

1. Fret groupé par container (LCL)
2. Fret maritime en conteneurs complets (FCL)
3. Fret en vrac
4. Transports de véhicules
5. Cargaison exceptionnelle ou projets industriels

#### Optimisation de votre transport par voie marine

![Chart](../../images/service/maritime-3.png)

